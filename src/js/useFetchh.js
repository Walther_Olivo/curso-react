import { useState, useEffect } from "react";

export function useFetchh(url) {
  const [data, setData] = useState([]);
  // tanto useState como useEffect son hooks en React
  // useState: Para manejar el estado en componentes funcionales
  // useEffect: Para manejar efectos secundarios (ciclo de vida)

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => setData(data))
      .catch((error) => console.error(error));
  }, []); // el [] significa que solo se ejecutará una vez, al montar el componente

  return { data };
}
