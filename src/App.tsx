import Titulo from "./Titulo";
import { useFetchh } from "./js/useFetchh";
import Card, { CardBody } from "./components/Card";
import List from "./components/List";
import Button from "./components/buttons/Button";
import { useState, useEffect } from "react";

function App() {
  //return <Card body={"d"} />;
  // const list = ["Keneth", "Rubén", "Angie"];
  // const list: string[] = [];
  // const handleSelect = (elemento: string) => {
  //   console.log("imprimiendo", elemento);
  // };
  // const content = list.length ? (
  //   <List data={list} onSelect={handleSelect}></List>
  // ) : (
  //   "No hay elementos"
  // );
  // return (
  //   <Card>
  //     <CardBody Titulole="Notes" text="Today was a day..." />
  //     {/* <List data={list} onSelect={handleSelect}></List> */}
  //     {content}
  //     <Button isLoading={false}>Hola</Button>
  //   </Card>
  // );¨
  // --
  // const [data, setData] = useState(["Goku", "Tanjiro", "Chanchito triste"]);
  // const addMinion = () => setData([...data, "Minion"]);
  // const deleteMinion = () => setData(data.slice(0, -1));
  // return (
  //   <Card>
  //     <Button onClick={addMinion}>Add</Button>
  //     <Button onClick={deleteMinion}>Delete</Button>
  //     <List data={data}></List>
  //   </Card>
  // );

  // const [data, setData] = useState<any>(null); // tanto useState como useEffect son hooks en React
  // // useState: Para manejar el estado en componentes funcionales
  // // useEffect: Para manejar efectos secundarios (ciclo de vida)

  // useEffect(() => {
  //   fetch("https://fakestoreapi.com/products")
  //     .then((response) => response.json())
  //     .then((data) => setData(data))
  //     .catch((error) => console.error(error));
  // }, []); // el [] significa que solo se ejecutará una vez, al montar el componente

  //   const { data } = useFetchh("https://fakestoreapi.com/products"); // tanto useState como useEffect son hooks en React

  //   return (
  //     <div className="container">
  //       <div className="row">
  //         {data ? (
  //           data.map((product: any) => (
  //             <div className="col-md-4" key={product.id}>
  //               <div
  //                 className="card"
  //                 style={{ width: "18rem", marginBottom: "20px" }}
  //               >
  //                 <img
  //                   src={product.image}
  //                   className="card-img-top"
  //                   alt={product.Titulole}
  //                   style={{ height: "200px", objectFit: "contain" }}
  //                 />
  //                 <div className="card-body">
  //                   <h5 className="card-Titulole">{product.Titulole}</h5>
  //                   <p className="card-text">{product.description}</p>
  //                   <p className="card-text">
  //                     <strong>Price:</strong> ${product.price}
  //                   </p>
  //                   <p className="card-text">
  //                     <strong>Rating:</strong> {product.rating.rate} (
  //                     {product.rating.count} reviews)
  //                   </p>
  //                   <a href="#" className="btn btn-primary">
  //                     Buy Now
  //                   </a>
  //                 </div>
  //               </div>
  //             </div>
  //           ))
  //         ) : (
  //           <p>Loading...</p>
  //         )}
  //       </div>
  //     </div>
  //   );

  return (
    <div>
      <h1>Bienvenido a mi aplicación de react!</h1>
      <Titulo></Titulo>
    </div>
  );
}

export default App;
