import { Children, ReactNode } from "react";
import "./Button.css";
type Props = {
  children: ReactNode;
  isLoading?: boolean;
  onClick?: () => void;
};

// inlineStyles
const styles = {
  border: "solid",
  border_radius: "30",
};

function Button({ children, isLoading, onClick }: Props) {
  return (
    <button
      // style={styles}
      onClick={onClick}
      disabled={isLoading}
      type="button"
      className="btn btn-success"
    >
      {isLoading ? "Loading..." : children}
    </button>
  );
}

export default Button;
