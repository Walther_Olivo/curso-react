import { useState } from "react";

type Props = {
  data: string[];
  onSelect?: (elemento: string) => void;
};

function List({ data, onSelect }: Props) {
  // const handlerEvent = (e: string) => {
  //   alert("Hola " + e);
  // };
  const [index, setIndex] = useState(1);
  const handlerEvent = (i: number, elemento: string) => {
    setIndex(i);
    onSelect?.(elemento);
  };

  return (
    <ul className="list-group">
      {data.map((elemento, i) => (
        <li
          onClick={() =>
            // alert("Hola " + elemento)
            // handlerEvent(elemento)
            handlerEvent(i, elemento)
          }
          key={elemento}
          className={`list-group-item ${index == i ? "active" : ""}`}
        >
          {elemento}
        </li>
      ))}
    </ul>
  );
}

export default List;
