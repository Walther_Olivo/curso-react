import { ReactNode } from "react";

interface Props {
  children?: ReactNode;
  //body?: string;
}

function Card(props: Props) {
  // const { body } = props;
  const { children } = props;

  return (
    <div
      className="card"
      style={{
        width: "340px",
      }}
    >
      <div className="card-body">
        {children}
        {/* {<CardBody></CardBody>} */}
      </div>
    </div>
  );
}

interface CarBodyProps {
  title?: string;
  text?: string;
}

export function CardBody(props: CarBodyProps) {
  const { title, text } = props;

  return (
    <> // Indicar un Fragment 
      <h5 className="card-title">{title}</h5>
      <p className="card-text">{text}</p>
      <a href="#" className="btn btn-primary">
        Go somewhere
      </a>
    </>
  );
}

export default Card;
